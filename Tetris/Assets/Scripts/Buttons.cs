using UnityEngine;
using UnityEngine.SceneManagement;

public class Buttons : MonoBehaviour
{
    public Sprite press, unpress;
    public GameObject mute, unmute;

    private void Start()
    {
        if(gameObject.name == "Music")
        {
            if (PlayerPrefs.GetString("Music") == "mute")
            {
                GameObject.Find("Canvas").GetComponent<AudioSource>().Pause();
                mute.SetActive(true);
                unmute.SetActive(false);
            }
            else
            {
                mute.SetActive(false);
                unmute.SetActive(true);
            }
        }
    }

    private void OnMouseDown()
    {
        GetComponent<SpriteRenderer>().sprite = press;
    }
    public void ButtonSound()
    {
        Playground.PlaySound(GetComponent<AudioSource>());
    }

    private void OnMouseUp()
    {
        GetComponent<SpriteRenderer>().sprite = unpress;
    }

    private void OnMouseUpAsButton()
    {
        if (Playground.Pause)
        {
            return;
        }
        ButtonSound();
        switch (gameObject.name)
        {
            case "Play":
                SceneManager.LoadScene("Play");
                break;
            case "Music":
                if(PlayerPrefs.GetString("Music") != "mute")
                {
                    PlayerPrefs.SetString("Music", "mute");
                    GameObject.Find("Canvas").GetComponent<AudioSource>().Pause();
                    mute.SetActive(true);
                    unmute.SetActive(false);
                } else
                {
                    GetComponent<AudioSource>().Play();
                    PlayerPrefs.SetString("Music", "unmute");
                    GameObject.Find("Canvas").GetComponent<AudioSource>().Play();
                    mute.SetActive(false);
                    unmute.SetActive(true);
                }
                break;
            case "Pause":
                    Playground.SetPause();
                    break;
        }
    }
}
