using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tetromino : MonoBehaviour
{
    public static Transform tetromino;
    private int movement = 1;
    private Vector3[] VectorMove;
    private static int[,,] tetrorot;
    private int rotate;

    void Start()
    {
        tetromino = Playground.tetromino.transform.GetChild(0);
        rotate = 0;
        TetrominoType.tetrominoType.TryGetValue(tetromino.name, out tetrorot);
    }

    private bool validPosition(Vector3[] vector)
    {
        int xPos;
        int yPos;

        for (int i = 0; i < tetromino.childCount; i++)
        {
            xPos = (int)(tetromino.GetChild(i).localPosition.x + vector[Mathf.Min(vector.Length - 1, i)].x);
            yPos = (int)(tetromino.GetChild(i).localPosition.y + vector[Mathf.Min(vector.Length - 1, i)].y);

            if (xPos < Playground.MinX || xPos > Playground.MaxX || yPos < Playground.MinY)
            {
                return false;
            }

            xPos = xPos - Playground.MinX;
            yPos = Mathf.Abs(yPos - (Playground.height + Playground.MinY - 1));

            if (Playground.grid[xPos, yPos] != null)
            {
                return false;
            }
        }
        return true;
    }

    public void LRotation()
    {
        if (rotate == 0)
        {
            rotate = 3;
        }
        else
        {
            rotate--;
        }

        VectorMove = new Vector3[4];
        for (int i = 0; i < tetromino.childCount; i++)
        {
            VectorMove[i] = new Vector3(-tetrorot[rotate, i, 0], -tetrorot[rotate, i, 1], 0);
        }
        if (validPosition(VectorMove))
        {
            for (int i = 0; i < tetromino.childCount; i++)
            {
                tetromino.GetChild(i).localPosition += VectorMove[i];
            }
        }
        else
        {

            if (rotate == 3)
            {
                rotate = 0;
            }
            else
            {
                rotate++;
            }
        }
    }
    public void RRotation()
    {
        VectorMove = new Vector3[4];
        for (int i = 0; i < tetromino.childCount; i++)
        {
            VectorMove[i] = new Vector3(tetrorot[rotate, i, 0], tetrorot[rotate, i, 1], 0);
        }
        if (validPosition(VectorMove))
        {
            for (int i = 0; i < tetromino.childCount; i++)
            {
                tetromino.GetChild(i).localPosition += VectorMove[i];
            }

            if (rotate == 3)
            {
                rotate = 0;
            }
            else
            {
                rotate++;
            }
        }
    }

    public void Left()
    {
        VectorMove = new Vector3[1] { new Vector3(-movement, 0, 0) };

        if (validPosition(VectorMove))
        {
            foreach (Transform child in tetromino)
            {
                child.localPosition += VectorMove[0];
            }
        }
    }
    public void Right()
    {
        VectorMove = new Vector3[1] { new Vector3(movement, 0, 0) };

        if (validPosition(VectorMove))
        {
            foreach (Transform child in tetromino)
            {
                child.localPosition += VectorMove[0];
            }
        }
    }
    public bool Down()
    {
        VectorMove = new Vector3[1] { new Vector3(0, -movement, 0) };

        if (validPosition(VectorMove))
        {
            foreach (Transform child in tetromino)
            {
                child.localPosition += VectorMove[0];
            }
        }
        else
        {
            Playground.NewTetromino();
            Playground.TimerUpdate();
            return false;
        }
        return true;
    }

    public void ChildDown(Transform child)
    {
        child.localPosition += new Vector3(0, -movement, 0);
    }

    public void DDown()
    {
        while (Down()) ;
    }
}
