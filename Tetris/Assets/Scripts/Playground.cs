using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Playground : MonoBehaviour
{
    private static GameObject GamePausePanel;
    private static GameObject GameOverPanel;
    private static GameObject ScoreText;
    private static GameObject HightScore;

    public const int height = 20;
    public const int width = 10;
    public const int MaxX = width/2;
    public const int MinX = -MaxX + 1;
    public const int MinY = -height + 2;
    public const int MaxTetrominoes = 7;

    private static float Timer;
    private static float DownTimeCD;
    private static float SpeedUpCD;
    private static float DownTime;
    private static int Score;
    private static bool NewHightScore;

    public static Tetromino tetromino;
    public static Creator creator;
    public static Transform[,] grid;
    public static bool Pause;

    void Start()
    {
        Pause = false;
        NewHightScore = false;
        Score = 0;
        Timer = 0;
        DownTimeCD = 0;
        SpeedUpCD = 0;
        DownTime = 1f;
        grid = new Transform[width, height];
        GamePausePanel = GameObject.Find("GamePause");
        GamePausePanel.SetActive(false);
        GameOverPanel = GameObject.Find("GameOver");
        GameOverPanel.SetActive(false);
        ScoreText = GameObject.Find("Score");
        HightScore = GameObject.Find("HightScore");
        HightScore.SetActive(false);
        creator = FindObjectOfType<Creator>();
        TetrominoType.clearTetrominoes();
        TetrominoType.addTetrominoes();
        tetromino = creator.Create();
    }

    public static void NewTetromino()
    {
        foreach (Transform child in Tetromino.tetromino)
        {
            int x = (int)child.localPosition.x - MinX;
            int y = (int)Mathf.Abs(child.localPosition.y - (height + MinY - 1));
            if (grid[x, y] == null)
            {
                grid[x, y] = child;
            }
            else
            {
                GameOver();
                return;
            }
        }
        checkLines();
        tetromino = creator.Create();
    }

    private static void checkLines()
    {
        foreach (Transform child in Tetromino.tetromino)
        {
            int x = (int)child.localPosition.x - MinX;
            int y = (int)Mathf.Abs(child.localPosition.y - (height + MinY - 1));
            if (checkLine(y))
            {
                removeLine(y);
                downLine(y);
            }
        }
    }

    public static void PlaySound(Component audio)
    {
        if (PlayerPrefs.GetString("Music") != "mute")
        {
            ((AudioSource) audio).Play();
        }
    }

    private static void GameOver()
    {
        Pause = true;
        GameObject.Find("Canvas").GetComponent<AudioSource>().Pause();
        ScoreText.SetActive(false);
        HightScore.SetActive(false);
        GameOverPanel.SetActive(true);
        PlaySound(GameOverPanel.GetComponent<AudioSource>());
        if (NewHightScore)
        {
            PlayerPrefs.SetInt("HightScore", Score);
            GameObject.Find("OverHightScore").SetActive(true);
        }
        else
        {
            GameObject.Find("OverHightScore").SetActive(false);
        }
        GameObject.Find("OverScore").GetComponent<Text>().text = Score.ToString();
    }

    private static bool checkLine(int y)
    {
        for (int i = 0; i < width; i++)
        {
            if (grid[i, y] == null)
            {
                return false;
            }
        }

        return true;
    }

    private static void removeLine(int y)
    {
        PlaySound(GameObject.Find("Playground").GetComponents<AudioSource>()[0]);
        for (int i = 0; i < width; i++)
        {
            Destroy(grid[i, y].gameObject);
            grid[i, y] = null;
        }
        AddScore();
    }
    private static void downLine(int y)
    {
        for (int i = y - 1; i >= 0; i--)
        {
            for (int j = 0; j < width; j++)
            {
                if (grid[j, i] != null)
                {
                    grid[j, i + 1] = grid[j, i];
                    grid[j, i] = null;
                    tetromino.ChildDown(grid[j, i + 1].transform);
                }
            }
        }
    }
    private static void AddScore()
    {
        if (TetrominoType.ActiveTetrominoes.Count == MaxTetrominoes)
        {
            Score += 10;
        }
        else
        {
            Score += TetrominoType.ActiveTetrominoes.Count;
        }
        ScoreText.GetComponent<Text>().text = Score.ToString();
        if (Score > PlayerPrefs.GetInt("HightScore") && !NewHightScore)
        {
            NewHightScore = true;
            PlaySound(GameObject.Find("Playground").GetComponents<AudioSource>()[2]);
            HightScore.SetActive(true);
        }
    }

    public static void SetPause()
    {
        Pause = true;
        GameObject.Find("Canvas").GetComponent<AudioSource>().Pause();
        GamePausePanel.SetActive(true);
    }

    public static void UnPause()
    {
        GamePausePanel.SetActive(false);
        PlaySound(GameObject.Find("Canvas").GetComponent<AudioSource>());
        Pause = false;
    }

    public static void exit()
    {
        SceneManager.UnloadSceneAsync("Play");
        Pause = false;
        SceneManager.LoadScene("Main");
    }

    public static void reload()
    {
        SceneManager.UnloadSceneAsync("Play");
        Pause = false;
        SceneManager.LoadScene("Play");
    }

    public static void TimerUpdate()
    {
        DownTimeCD = Timer;
    }

    void Update()
    {
        if (Pause)
        {
            return;
        }
        Timer += Time.deltaTime;
        if ((Timer - SpeedUpCD) > 60f)
        {
            SpeedUpCD = Timer;
            DownTime *= 0.75f;
            PlaySound(GameObject.Find("Playground").GetComponents<AudioSource>()[1]);
        }
        if ((Timer - DownTimeCD) > DownTime)
        {
            TimerUpdate();
            tetromino.Down();
        }
    }
}
