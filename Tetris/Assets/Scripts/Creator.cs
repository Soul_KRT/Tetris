using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Creator : MonoBehaviour
{

    public Tetromino[] Tetrominoes;
   
    public Tetromino Create()
    {
        return Instantiate(TetrominoType.ActiveTetrominoes[Random.Range(0, TetrominoType.ActiveTetrominoes.Count)], transform.position, Quaternion.identity, GameObject.Find("Canvas").transform);
    }
}
