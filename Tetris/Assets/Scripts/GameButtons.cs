using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class GameButtons : MonoBehaviour
{
    public Sprite press, unpress;
    private static float ClickT;
    public static float ClickCD = 0.15f;

    private static bool KrissCheck()
    {
        return PlayerPrefs.GetString("nick").ToLower().Equals("kriskent") && Random.Range(0f, 1f) >= 0.5;
    }

    private void OnMouseDown()
    {
        if (Playground.Pause)
        {
            return;
        }
        if (KrissCheck()) return;
        GetComponent<SpriteRenderer>().sprite = press;
        if (!(gameObject.name == "Left" || gameObject.name == "Right" || gameObject.name == "Down"))
        {
            Playground.PlaySound(GetComponent<AudioSource>());
            switch (gameObject.name)
            {
                case "LRotation":
                    Playground.tetromino.LRotation();
                    break;
                case "RRotation":
                    Playground.tetromino.RRotation();
                    break;
                case "DDown":
                    Playground.tetromino.DDown();
                    break;
            }
        }
    }

    private void OnMouseDrag()
    {
        if (Playground.Pause)
        {
            return;
        }
        if (gameObject.name == "Left" || gameObject.name == "Right" || gameObject.name == "Down")
        {
            if ((Time.time - ClickT) > ClickCD)
            {
                ClickT = Time.time;
                if (KrissCheck()) return;
                Playground.PlaySound(GetComponent<AudioSource>());
                switch (gameObject.name)
                {
                    case "Left":
                        Playground.tetromino.Left();
                        break;
                    case "Right":
                        Playground.tetromino.Right();
                        break;
                    case "Down":
                        Playground.tetromino.Down();
                        break;
                }
            }
        }
    }

    private void OnMouseUp()
    {
        GetComponent<SpriteRenderer>().sprite = unpress;
    }
}
