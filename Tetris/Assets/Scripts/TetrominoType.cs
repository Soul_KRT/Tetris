using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TetrominoType : MonoBehaviour
{
    public static List<Tetromino> ActiveTetrominoes = new List<Tetromino>();
    public static Dictionary<string, int[,,]> tetrominoType = new Dictionary<string, int[,,]>();

    public static void addTetrominoes()
    {
        if (PlayerPrefs.GetInt("Tetromino0") == 1)
        {
            ActiveTetrominoes.Add(Playground.creator.Tetrominoes[0]);
            tetrominoType.Add("I",
                new int[4, 4, 2] {
                    { { 0, 0 }, { -1, -1 }, { 1, 1 }, { -2, -2 } },
                    { { 0, 0 }, { -1, 1 }, { 1, -1 }, { -2, 2 } },
                    { { 0, 0 }, { 1, 1 }, { -1, -1 }, { 2, 2 } },
                    { { 0, 0 }, { 1, -1 }, { -1, 1 }, { 2, -2 } }
                });
        }
        if (PlayerPrefs.GetInt("Tetromino1") == 1)
        {
            ActiveTetrominoes.Add(Playground.creator.Tetrominoes[1]);
            tetrominoType.Add("J",
                new int[4, 4, 2] {
                    { { 0, 0 }, { -1, -1 }, { 2, 0 }, { 1, 1 } },
                    { { 0, 0 }, { -1, 1 }, { 0, -2 }, { 1, -1 } },
                    { { 0, 0 }, { 1, 1 }, { -2, 0 }, { -1, -1 } },
                    { { 0, 0 }, { 1, -1 }, { 0, 2 }, { -1, 1 } }
                });
        }
        if (PlayerPrefs.GetInt("Tetromino2") == 1)
        {
            ActiveTetrominoes.Add(Playground.creator.Tetrominoes[2]);
            tetrominoType.Add("L",
                new int[4, 4, 2] {
                    { { 0, 0 }, { -1, -1 }, { 0, -2 }, { 1, 1 }, },
                    { { 0, 0 }, { -1, 1 }, { -2, 0 }, { 1, -1 }, },
                    { { 0, 0 }, { 1, 1 }, { 0, 2 }, { -1, -1 }, },
                    { { 0, 0 }, { 1, -1 }, { 2, 0 }, { -1, 1 }, },
                });
        }

        if (PlayerPrefs.GetInt("Tetromino3") == 1)
        {
            ActiveTetrominoes.Add(Playground.creator.Tetrominoes[3]);
            tetrominoType.Add("O",
                new int[4, 4, 2] {
                    { { 0, 0 }, { -1, -1 }, { 0, -2 }, { 1, -1 } },
                    { { 0, 0 }, { -1, 1 }, { -2, 0 }, { -1, -1 } },
                    { { 0, 0 }, { 1, 1 }, { 0, 2 }, { -1, 1 } },
                    { { 0, 0 }, { 1, -1 }, { 2, 0 }, { 1, 1 } }
                });
        }
        if (PlayerPrefs.GetInt("Tetromino4") == 1)
        {
            ActiveTetrominoes.Add(Playground.creator.Tetrominoes[4]);
            tetrominoType.Add("S",
                new int[4, 4, 2] {
                    { { 0, 0 }, { 1, 1 }, { 0, -2 }, { 1, -1 } },
                    { { 0, 0 }, { 1, -1 }, { -2, 0 }, { -1, -1 } },
                    { { 0, 0 }, { -1, -1 }, { 0, 2 }, { -1, 1 } },
                    { { 0, 0 }, { -1, 1 }, { 2, 0 }, { 1, 1 } }
                });
        }
        if (PlayerPrefs.GetInt("Tetromino5") == 1)
        {
            ActiveTetrominoes.Add(Playground.creator.Tetrominoes[5]);
            tetrominoType.Add("T",
                new int[4, 4, 2]{
                    { { 0, 0 }, { -1, -1 }, { 1, 1 }, { 1, -1 } },
                    { { 0, 0 }, { -1, 1 }, { 1, -1 }, { -1, -1 } },
                    { { 0, 0 }, { 1, 1 }, { -1, -1 }, { -1, 1 } },
                    { { 0, 0 }, { 1, -1 }, { -1, 1 }, { 1, 1 } }
                });
        }
        if (PlayerPrefs.GetInt("Tetromino6") == 1)
        {
            ActiveTetrominoes.Add(Playground.creator.Tetrominoes[6]);
            tetrominoType.Add("Z",
                new int[4, 4, 2] {
                    { { 0, 0 }, { -1, -1 }, { 2, 0 }, { 1, -1 } },
                    { { 0, 0 }, { -1, 1 }, { 0, -2 }, { -1, -1 } },
                    { { 0, 0 }, { 1, 1 }, { -2, 0 }, { -1, 1 } },
                    { { 0, 0 }, { 1, -1 }, { 0, 2 }, { 1, 1 } }
                });
        }
    }

    public static void clearTetrominoes()
    {
        tetrominoType.Clear();
        ActiveTetrominoes.Clear();
    }
}
