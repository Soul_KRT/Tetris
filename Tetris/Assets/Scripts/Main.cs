using UnityEngine;
using UnityEngine.UI;

public class Main : MonoBehaviour
{
    public GameObject HightScore;
    public GameObject NickPanel;
    public GameObject Warning;
    public GameObject NickInput;
    public GameObject Nickname;
    public GameObject Cancel;
    public GameObject SettingsPanel;
    public GameObject SettingsNick;
    public GameObject TetrominoesPanel;
    public Toggle[] Tetrominoes;
    public GameObject SettingsWarning;

    private static int activeTetrominoes;

    void Start()
    {
        SettingsPanel.SetActive(false);
        NickPanel.SetActive(false);
        Cancel.SetActive(false);
        HightScore.GetComponent<Text>().text = PlayerPrefs.GetInt("HightScore").ToString();
        if(PlayerPrefs.GetString("nick").Length < 4 || PlayerPrefs.GetString("nick").Length > 12)
        {
            Playground.Pause = true;
            NickPanel.SetActive(true);
            return;
        }
        Nickname.GetComponent<Text>().text = "Добро пожаловать,\n" + PlayerPrefs.GetString("nick") + "!";
    }
    private void InputNick()
    {
        string nick = NickInput.GetComponent<Text>().text;
        if(nick.Length < 4 || nick.Length > 12)
        {
            Warning.GetComponent<Text>().text = "Длинна ника должна быть\nот 4 до 12 символов!";
            return;
        }
        PlayerPrefs.SetString("nick", nick);
        NickPanel.SetActive(false);
        Nickname.GetComponent<Text>().text = "Добро пожаловать,\n" + PlayerPrefs.GetString("nick") + "!";
        if (SettingsPanel.activeSelf)
        {
            SettingsNick.GetComponent<Text>().text = "Ник:\n" + PlayerPrefs.GetString("nick");
            TetrominoesPanel.SetActive(true);
        } else
        {
            Playground.Pause = false;
            for(int i = 0; i < Tetrominoes.Length; i++)
            {
                PlayerPrefs.SetInt("Tetromino" + i, 1);
            }
        }
    }

    private void OpenSettings()
    {
        if (Playground.Pause)
        {
            return;
        }
        Playground.Pause = true;
        SettingsPanel.SetActive(true);
        SettingsWarning.SetActive(false);
        SettingsNick.GetComponent<Text>().text = "Ник:\n" + PlayerPrefs.GetString("nick");
        activeTetrominoes = 7;
        for (int i = 0; i < Tetrominoes.Length; i++)
        {
            if(PlayerPrefs.GetInt("Tetromino" + i) == 0)
            {
                Tetrominoes[i].isOn = false;
            }
        }
    }

    private void Change()
    {
        TetrominoesPanel.SetActive(false);
        NickPanel.SetActive(true);
        Cancel.SetActive(true);
    }

    private void CloseNickPanel()
    {
        Cancel.SetActive(false);
        NickPanel.SetActive(false);
        TetrominoesPanel.SetActive(true);
    }

    private void CloseSettings()
    {
        SettingsPanel.SetActive(false);
        Playground.Pause = false;
    }

    private void ChangeValue(int key)
    {
        if (Tetrominoes[key].isOn == false)
        {
            activeTetrominoes--;
            if (activeTetrominoes == 2)
            {
                SettingsWarning.SetActive(true);
                Tetrominoes[key].isOn = true;
                return;
            }
            PlayerPrefs.SetInt("Tetromino" + key, 0);
        }
        else
        {
            activeTetrominoes++;
            if (activeTetrominoes == 4)
            {
                SettingsWarning.SetActive(false);
            }
            PlayerPrefs.SetInt("Tetromino" + key, 1);
        }
    }
}
